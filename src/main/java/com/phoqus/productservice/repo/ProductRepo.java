package com.phoqus.productservice.repo;

import com.phoqus.productservice.data.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepo extends JpaRepository<Product, Long> {

}
