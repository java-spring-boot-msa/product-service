package com.phoqus.productservice.service;

import com.phoqus.productservice.data.Product;
import com.phoqus.productservice.repo.ProductRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductService {
    @Autowired
    private ProductRepo productRepo;

    public Product createUser(Product product) {
        return productRepo.save(product);
    }

    public Product getUserById(Long id) {
        Optional<Product> product= productRepo.findById(id);
        if(product.isPresent()){
            return product.get();
        }
        return null;
    }

    public Product updateUser(Product product) {
        return productRepo.save(product);
    }

    public void deleteUser(Long id) {
        productRepo.deleteById(id);
    }

    public Page<Product> getAll(Pageable pageable) {
        return productRepo.findAll(pageable);
    }
}
