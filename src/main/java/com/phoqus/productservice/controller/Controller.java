package com.phoqus.productservice.controller;

import com.phoqus.productservice.data.Product;
import com.phoqus.productservice.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/product")
public class Controller {
    @Autowired
    ProductService productService;

    @GetMapping("/hello")
    public String health(){
        return "Hello World! ProductServiceApplication";
    }
    @PostMapping
    public Product createUser(@RequestBody Product product) {
        return productService.createUser(product);
    }

    @GetMapping("/{id}")
    public Product getUserById(@PathVariable Long id) {
        return productService.getUserById(id);
    }
    @GetMapping("/all")
    public Page<Product> getProducts(Pageable pageable) {
        return productService.getAll(pageable);
    }
    @PutMapping
    public Product updateUser(@RequestBody Product product) {
        return productService.updateUser(product);
    }
}
